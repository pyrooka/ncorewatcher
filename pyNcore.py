#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
TODO:
-oldalak vegignezesenel threadeket alkalmazni...5-10 thread jelentosen meggyorsitana!!!!!!
-hibak kezelese: nincs talalat
-ha nem talalja a taget akkor maskepp kell rakeresni
-timert berakni, hogy ne dobjon ki

FONTOS bar ez mar webes resze: ha kulso IProl csatlakoznak, akkor kerjen jelszot!!
"""

import requests
from bs4 import BeautifulSoup
import os # for clear the console


url_login = "https://ncore.cc/login.php"
url_index = "https://ncore.cc/index.php"
url_torrent = "https://ncore.cc/torrents.php"
header = {"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36"}


class TorrentType:
    """
    ncoren kereseshez tipusok, amiket hasznalni fogok
    """
    #minden
    minden = "all_own"
    #filmek
    film_magyar_sima = "xvid_hun"
    film_angol_sima = "xvid"
    film_magyar_hd = "hd_hun"
    film_angol_hd = "hd"
    #sorozatok
    sorozat_magyar_sima = "xvidser_hun"
    sorozat_angol_sima = "xvidser"
    sorozat_magyar_hd = "hdser_hun"
    sorozat_angol_hd = "hdser"
    #zenek
    zene_magyar_mp3 = "mp3_hun"
    zene_angol_mp3 = "mp3"
    zene_magyar_loseless = "loseless_hun"
    zene_angol_loseless = "loseless"
    #programok
    program_iso = "iso"
    program_rip = "misc"
    program_mobile = "mobil"
    #ekonyvek
    ekonyv_magyar = "ebook_hun"
    ekonyv_angol = "ebook"

class Torrent:
    """
    egy torrent objektum
    """
    name = ""
    date = ""
    size = ""
    downloaded = ""
    seed = ""
    leech = ""
    link = ""

    def __init__(self, name, date, size, downloaded, seed, leech, link):
        self.name = name
        self.date = date
        self.size = size
        self.downloaded = downloaded
        self.seed = seed
        self.leech = leech
        self.link = link

    def printLine(self):
         print self.name
         print "Size: " + self.size + "\tD: " + self.downloaded + "\tS: " + self.seed + "\tL: " + self.leech + "\t" + self.date
         print "| " + self.link

class NcoreBrowser():
    """
    Ez a fo osztaly. ezen belul van minden mas vagy wtf
    """
    #felhasznalonev
    username = ""
    #session
    session = None
    #mindig a legutolso response
    result = None
    #how many page will be search for tags
    pageNumber = 10
    #torrent list. type:Torrent
    torrents = []

    #contructor
    def __init__(self):

        self.session = requests.Session()

    #handle the login process
    def login(self, username, password):
        """
        belepes es session tarolasa
        """
        #save username if later wanna use
        self.username = username

        #post request to login
        self.result = self.session.post(url_login, data={'nev': username, 'pass': password, "ne_leptessen_ki":"1"}, headers=header, verify = True)

        import threading
        threading._sleep(1)

        #get request to get the deafult index page after login
        self.result = self.session.get(url_index)

        #check login
        return self.checkLogin()

    #searching method
    def search(self, type, name):
        """
        get result page from search
        :param type: a type from the "Type" class
        :param name: the torrent name u want to find
        :return: torrent list if okay
        """

        self.result = self.session.get(url_torrent + "?tipus=" + type + "&mire=" + name)

        if(self.isAnyResult()):
            pages = self.getPageNumbers(type, name)
            #iterate in the pages and return the torrents data
            for page in range(1,pages):

                self.torrents.extend(self.getTorrents(type, name, page))

            return self.torrents
        else:
            return False

    #check if the search has any result
    def isAnyResult(self):
        """
        just check if are any torrent in the result html page
        :return: true if have result, false if dont
        """

        #parsed result page
        parsed = BeautifulSoup(self.result.content, "html.parser")
        listt = parsed.find_all("div")
        #if doesnt find anythting return false
        for tag in parsed.find_all("div"):
            if u"class" in tag.attrs and tag.attrs[u"class"][0] == u"lista_mini_error":
                if tag.text == u"Nincs találat!":
                    return False
                else:
                    return True
        return True

    #return the number of the pages of a search
    def getPageNumbers(self, type, name):
        """
        this functions is return the page's number of the search result
        :return: number of the page
        """

        #this is the default number of the pages
        numbers = 1

        #lets try to iterate through the pages
        if self.pageNumber not in range(1,100): self.pageNumber = 1 #check is page number correct
        for i in range(0,self.pageNumber):
            self.result = self.session.get(url_torrent + "?tipus=" + type + "&mire=" + name + "&oldal=" + str(i+1))
            if self.isAnyResult():
                numbers += 1
            else:
                break


        return numbers

    #return a torrent(type:Torrent) object from a div:box_nagy(2) tag
    def createTorrent(self, tag):

        name = tag.contents[1].text[4:-4].strip("\n")

        uploaded = tag.contents[5].text
        uploaded = uploaded[:10]+" "+uploaded[10:]
        size = tag.contents[9].text
        downloaded = tag.contents[13].text
        seed = tag.contents[17].text
        leech = tag.contents[21].text
        try:
            link = "https://ncore.cc/" + tag.contents[1].contents[3].contents[3].contents[1].attrs[u"href"].replace("details", "download")
        except:
            try:
                link = "https://ncore.cc/" + tag.contents[1].contents[1].contents[3].attrs[u"href"].replace("details", "download")
            except:
                    try:
                        link = "https://ncore.cc/" + tag.contents[1].contents[1].contents[3].contents[1].attrs[u"href"].replace("details", "download")
                    except:
                        link = "ERROR: Cannot find the content in the tag."

        return Torrent(name, uploaded, size, downloaded, seed, leech, link)

    #return the list of the torrents
    def getTorrents(self, type, name, page):
        """
        get the torrents data from the actual page. returns a list of Torrent object
        :return: a list of Torrent
        """
        #return this torrent list
        torrents = []
        #get the page
        result = self.session.get(url_torrent + "?tipus=" + type + "&mire=" + name + "&oldal=" + str(page))
        #parsed actual result
        parsed = BeautifulSoup(result.content, "html.parser")

        divlist = parsed.find_all("div")

        for tag in parsed.find_all("div"):
            if u"class" in tag.attrs and (tag.attrs[u"class"] == [u"box_nagy"] or tag.attrs[u"class"] == [u"box_nagy2"]):

                torrents.append(self.createTorrent(tag))

        return torrents

    #simply refresh the page
    def refresh(self):
        self.result = self.session.get(url_index)
        return self.checkLogin()

    #check login state
    def checkLogin(self):

        #parsed html to get the title
        parsedResult = BeautifulSoup(self.result.content,"html.parser")

        #return if title is equal to the unicode mainpage title
        if(parsedResult.find("head").find("title").text == u"nCore | Főoldal"):
            return True
        else:
            return False



#####################################
#############__MAIN__################
#####################################

def clear():
    os.system("cls" if os.name == "nt" else "clear")

def setPageNumber(ncb):
    go = True
    while go:
        p = raw_input("Add meg hány oldalnyi találatot szeretnél megkapni: ")
        if type(p) is int and p > 0:
            ncb.pageNumber = p
            go = False
            print "Vizsgált oldalak száma: " + str(p)
            break

        else:
            print "Egynél nagyobb egész számot kell megadnod!"

def chooseCategory(types):

    clear()

    print "Kategóriák:"

    for t in range(1, len(types)):
        print "(" + str(t) + ") " + types[t-1]

    i = input("Válassz egyet: ")

    if i not in range(1,18):
        print "Hiba! Válassz újra!"

    print "Valasztott kategoria: " + types[i-1]
    return TorrentType.__dict__[types[i-1]]

def printHelp():
    print "Keresésnél az alábbi parancsokat használhatod:"
    print "/help - parancsok kilistázása"
    print "/list - kategóriák kiírása"
    print "/page - maximum keresett oldalak száma"
    print "/exit - kilépés a programból"

if __name__ == "__main__":

    import getpass

    clear()

    user = raw_input("Felhasználónév: ")
    passw = getpass.getpass("Jelszó: ")

    print("Belépés folyamatban....")

    nc = NcoreBrowser()

    login = nc.login(user, passw)

    if(login):
        print("Belépve.")
    else:
        print("Belépés sikertelen.")
        exit()



    types = TorrentType.__dict__.keys()
    types.remove("__module__")
    types.remove("__doc__")
    types.sort()

    kategoria = chooseCategory(types)

    printHelp()

    while True:

        n = raw_input("Keresés: ")

        if n == "/list":

            kategoria = chooseCategory(types)

        elif n == "/exit":
            exit()

        elif n == "/help":
            printHelp()

        elif n == "/page":
            setPageNumber(nc)

        else:

            result = nc.search(kategoria,n)

            if(result):

                for t in result:
                    print "| " + t.name.replace("\n", "\n|")
                    print "| " + "Size: " + t.size + "\tD: " + t.downloaded + "\tS: " + t.seed + "\tL: " + t.leech + "\t" + t.date
                    print "| " + t.link
                    print "+-----------------------------------------------------------------------"


            else:
                print("Nincs találat.")





